package Config;

import com.mysql.jdbc.*;
import java.sql.DriverManager;

public class Conection {
    Connection con;
    
    public Conection() {
        try{
            Class.forName("com.mysql.jdbc.Driver");
            con=(Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/sistema-mvc-java", "root", "");
        }catch(Exception e){
            System.err.println("Error: " + e);
        }
    }
    
    public Connection getConnection(){
        return con;
    }
}
