package ModelsDAO;

import Config.Conection;
import Interface.CRUD;
import Models.Pessoa;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class PessoaDAO implements CRUD {
    Conection cn = new Conection();
    Connection con;
    PreparedStatement ps;
    ResultSet rs;
    Pessoa p = new Pessoa();
    
    @Override
    public List all() {
        ArrayList<Pessoa>list = new ArrayList<>();
        String sql = "SELECT * FROM pessoas";
        try{
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while(rs.next()){
                Pessoa p = new Pessoa();
                p.setId(rs.getInt("id"));
                p.setNome(rs.getString("nome"));
                p.setEmail(rs.getString("email"));
                p.setTelefone(rs.getString("telefone"));
                list.add(p);
            }
        }catch(Exception e){
            
        }
        return list;
    }

    @Override
    public Pessoa find(int id) {
        String sql="select * from pessoas where Id="+id;
        try {
            con=cn.getConnection();
            ps=con.prepareStatement(sql);
            rs=ps.executeQuery();
            while(rs.next()){                
                p.setId(rs.getInt("id"));
                p.setNome(rs.getString("nome"));
                p.setTelefone(rs.getString("telefone"));
                p.setEmail(rs.getString("email"));
            }
        } catch (Exception e) {
        }
        return p;
    }

    @Override
    public boolean store(Pessoa pessoa) {
        String sql = "INSERT INTO pessoas (nome, telefone, email) values('"+pessoa.getNome()+"','"+pessoa.getTelefone()+"','"+pessoa.getEmail()+"')";
        try{
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.executeUpdate();
        }catch(Exception e){
            
        }
        return false;
    }

    @Override
    public boolean update(Pessoa pessoa) {
        String sql="UPDATE pessoas SET nome='"+pessoa.getNome()+"',telefone='"+pessoa.getTelefone()+"',email='"+pessoa.getEmail()+"'WHERE id="+pessoa.getId();
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e) {
        }
        return false;
    }

    @Override
    public boolean delete(int id) {
        String sql="delete from pessoas where id="+id;
        try {
            con=cn.getConnection();
            ps=con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e) {
        }
        return false;
    }
    
}
