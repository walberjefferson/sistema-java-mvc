
package ModelsDAO;

import Config.Conection;
import Interface.CRUDUsuario;
import Models.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class UsuarioDAO implements CRUDUsuario {
    Conection cn = new Conection();
    Connection con;
    PreparedStatement ps;
    ResultSet rs;
    Usuario u = new Usuario();

    @Override
    public List all() {
        ArrayList<Usuario>list = new ArrayList<>();
        String sql = "SELECT * FROM usuarios";
        try{
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while(rs.next()){
                Usuario u = new Usuario();
                u.setId(rs.getInt("id"));
                u.setEmail(rs.getString("email"));
                u.setSenha(rs.getString("senha"));
                list.add(u);
            }
        }catch(Exception e){
            
        }
        return list;
    }

    @Override
    public Usuario find(int id) {
        String sql="select * from usuarios where id="+id;
        try {
            con=cn.getConnection();
            ps=con.prepareStatement(sql);
            rs=ps.executeQuery();
            while(rs.next()){                
                u.setId(rs.getInt("id"));
                u.setEmail(rs.getString("email"));
                u.setSenha(rs.getString("senha"));
            }
        } catch (Exception e) {
        }
        return u;
    }

    @Override
    public boolean store(Usuario usuario) {
        String sql = "INSERT INTO usuarios (email, senha) values('"+usuario.getEmail()+"','"+usuario.getSenha()+"')";
        try{
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.executeUpdate();
        }catch(Exception e){
            
        }
        return false;
    }

    @Override
    public boolean update(Usuario usuario) {
        String sql="UPDATE usuarios SET nome='"+usuario.getEmail()+"',telefone='"+usuario.getSenha()+"' WHERE id="+usuario.getId();
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e) {
        }
        return false;
    }

    @Override
    public boolean delete(int id) {
        String sql="delete from usuarios where id="+id;
        try {
            con=cn.getConnection();
            ps=con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e) {
        }
        return false;
    }
    
}
