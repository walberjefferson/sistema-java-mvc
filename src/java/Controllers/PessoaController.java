package Controllers;

import Models.Pessoa;
import ModelsDAO.PessoaDAO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class PessoaController extends HttpServlet {

    String login = "index.jsp";
    String listar = "views/list.jsp";
    String create = "views/create.jsp";
    String edit = "views/edit.jsp";
    Pessoa p = new Pessoa();
    PessoaDAO dao = new PessoaDAO();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet PessoaController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet PessoaController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession sessao = request.getSession();
        Object val = sessao.getAttribute("usucodigo");
        String route = "";
        String action = request.getParameter("action");
        if(val!=null) //talvez ele não consiga comparar aqui
        {
            if(action.equalsIgnoreCase("listar")){
                route = listar;
            }else if(action.equalsIgnoreCase("create")){
                route = create;
            }else if(action.equalsIgnoreCase("Salvar")){
                String nome = request.getParameter("nome");
                String telefone = request.getParameter("telefone");
                String email = request.getParameter("email");
                p.setNome(nome);
                p.setTelefone(telefone);
                p.setEmail(email);
                dao.store(p);
                route = listar;
            }else if(action.equalsIgnoreCase("editar")){
                request.setAttribute("id", request.getParameter("id"));
                route = edit;
            }else if(action.equalsIgnoreCase("Atualizar")){
                Integer id = Integer.parseInt(request.getParameter("id"));
                String nome = request.getParameter("nome");
                String telefone = request.getParameter("telefone");
                String email = request.getParameter("email");
                p.setId(id);
                p.setNome(nome);
                p.setTelefone(telefone);
                p.setEmail(email);
                dao.update(p);
                route = listar;
            }else if(action.equalsIgnoreCase("delete")){
                Integer id=Integer.parseInt(request.getParameter("id"));
                p.setId(id);
                dao.delete(id);
                route = listar;
            }
            RequestDispatcher view = request.getRequestDispatcher(route);
            view.forward(request, response);
            //response.sendRedirect(listar);
        }
        else
        {
            response.sendRedirect(login); //ou ele não está achando o caminho
        }
        
        
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
