
package Controllers;

import Config.Conection;
import Models.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginController extends HttpServlet {
    Conection cn = new Conection();
    Connection con;
    ResultSet rs;
    String login = "index.jsp";


    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet LoginController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet LoginController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //String logout = request.getParameter('logout');
        String action = request.getParameter("action");
        HttpSession sessao = request.getSession();
        Object val = sessao.getAttribute("usucodigo");
        

        if(val!=null) //talvez ele não consiga comparar aqui
        {
            if(action.equalsIgnoreCase("logout")){
                sessao.invalidate();
                response.sendRedirect(login);
            }else{
                response.sendRedirect("pessoa?action=listar");   
            }
        }
        else
        {
            response.sendRedirect(login);
        }
        //processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        
        
        try {
            String email = request.getParameter("email");
            String senha = request.getParameter("senha");
            
            con=cn.getConnection();
            Statement stmt = con.createStatement();
            rs = stmt.executeQuery("select * from usuarios where email='"+email+"' and senha='"+senha+"'");

            if(rs.next()) {
              HttpSession session = request.getSession();
              session.setAttribute("usucodigo", senha);
              response.sendRedirect("pessoa?action=listar");
            }else {
                response.sendRedirect(login);
                //out.println("Wrong id and password");
            }
          } catch (SQLException ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
