
package Controllers;

import Models.Usuario;
import ModelsDAO.UsuarioDAO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class UsuarioController extends HttpServlet {

    String login = "index.jsp";
    String listar = "views/usuario/list.jsp";
    String create = "views/usuario/create.jsp";
    String edit = "views/usuario/edit.jsp";
    Usuario u = new Usuario();
    UsuarioDAO dao = new UsuarioDAO();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UsuarioController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UsuarioController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession sessao = request.getSession();
        Object val = sessao.getAttribute("usucodigo");
        String route = "";
        String action = request.getParameter("action");
        if(val!=null) //talvez ele não consiga comparar aqui
        {
            if(action.equalsIgnoreCase("listar")){
                route = listar;
            }else if(action.equalsIgnoreCase("create")){
                route = create;
            }else if(action.equalsIgnoreCase("Salvar")){
                String email = request.getParameter("email");
                String senha = request.getParameter("senha");
                u.setEmail(email);
                u.setSenha(senha);
                dao.store(u);
                route = listar;
            }else if(action.equalsIgnoreCase("editar")){
                request.setAttribute("id", request.getParameter("id"));
                route = edit;
            }else if(action.equalsIgnoreCase("Atualizar")){
                Integer id = Integer.parseInt(request.getParameter("id"));
                String email = request.getParameter("email");
                String senha = request.getParameter("senha");
                u.setId(id);
                u.setEmail(email);
                u.setSenha(senha);
                dao.update(u);
                route = listar;
            }else if(action.equalsIgnoreCase("delete")){
                Integer id=Integer.parseInt(request.getParameter("id"));
                u.setId(id);
                dao.delete(id);
                route = listar;
            }
            RequestDispatcher view = request.getRequestDispatcher(route);
            view.forward(request, response);
        }else
        {
            response.sendRedirect(login); //ou ele não está achando o caminho
        }
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
