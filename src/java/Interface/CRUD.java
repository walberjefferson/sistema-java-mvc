package Interface;

import Models.Pessoa;
import java.util.List;

public interface CRUD {
    public List all();
    public Pessoa find(int id);
    public boolean store(Pessoa pessoa);
    public boolean update(Pessoa pessoa);
    public boolean delete(int id);
}
