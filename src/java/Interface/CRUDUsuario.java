
package Interface;

import Models.Usuario;
import java.util.List;

public interface CRUDUsuario {
    public List all();
    public Usuario find(int id);
    public boolean store(Usuario usuario);
    public boolean update(Usuario usuario);
    public boolean delete(int id);
}
