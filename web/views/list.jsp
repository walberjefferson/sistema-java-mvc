<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="Models.Pessoa"%>
<%@page import="ModelsDAO.PessoaDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sistema de Cadastro de Pessoas</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="./css/bootstrap.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="container">
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                      <li class="nav-item active">
                        <a class="nav-link" href="./">Início <span class="sr-only">(current)</span></a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="pessoa?action=listar">Listar Pessoas</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="pessoa?action=create">Adicionar Pessoa</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="usuario?action=listar">Listar Usuários</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="usuario?action=create">Adicionar Usuário</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="login?action=logout">Sair</a>
                      </li>
                    </ul>
                </div>
            </div>
        </nav>
        
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="pt-4  pb-3">Listagem de Pessoas</h1>
                    <table class="table table-bordered table-sm">
                        <thead>
                            <tr>
                                <th class="text-center" width="4%">#</th>
                                <th>Nome</th>
                                <th>Telefone</th>
                                <th>E-mail</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                PessoaDAO dao = new PessoaDAO();
                                List<Pessoa> list = dao.all();
                                Iterator<Pessoa> itens = list.iterator();
                                Pessoa pessoa = null;
                                while(itens.hasNext()){
                                    pessoa = itens.next();
                            %>
                            <tr>
                                <td class="text-center"><%= pessoa.getId() %></td>
                                <td class=""><%= pessoa.getNome() %></td>
                                <td class="text-center" width="10%" nowrap><%= pessoa.getTelefone() %></td>
                                <td class="text-center" width="15%" nowrap><%= pessoa.getEmail() %></td>
                                <td class="" width="16%">
                                    <div class="btn-group btn-group-sm btn-block p-0 m-0">
                                        <a href="pessoa?action=editar&id=<%= pessoa.getId() %>" class="btn btn-info">Editar</a>
                                        <a href="pessoa?action=delete&id=<%= pessoa.getId() %>" class="btn btn-danger">Excluir</a>
                                    </div>
                                </td>
                            </tr>
                            <% } %>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>
