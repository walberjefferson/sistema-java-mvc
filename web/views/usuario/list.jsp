<%@page import="Models.Usuario"%>
<%@page import="ModelsDAO.UsuarioDAO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sistema de Cadastro de Pessoas</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="./css/bootstrap.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="container">
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                      <li class="nav-item active">
                        <a class="nav-link" href="./">Início <span class="sr-only">(current)</span></a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="pessoa?action=listar">Listar Pessoas</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="pessoa?action=create">Adicionar Pessoa</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="usuario?action=listar">Listar Usuários</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="usuario?action=create">Adicionar Usuário</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="login?action=logout">Sair</a>
                      </li>
                    </ul>
                </div>
            </div>
        </nav>
        
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="pt-4  pb-3">Listagem de Usuários</h1>
                    <table class="table table-bordered table-sm">
                        <thead>
                            <tr>
                                <th class="text-center" width="4%">#</th>
                                <th>E-mail</th>
                                <th>Senha</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                UsuarioDAO dao = new UsuarioDAO();
                                List<Usuario> list = dao.all();
                                Iterator<Usuario> itens = list.iterator();
                                Usuario usuario = null;
                                while(itens.hasNext()){
                                    usuario = itens.next();
                            %>
                            <tr>
                                <td class="text-center"><%= usuario.getId() %></td>
                                <td class="text-center" nowrap><%= usuario.getEmail() %></td>
                                <td class="text-center" width="15%" nowrap><%= usuario.getSenha()%></td>
                                <td class="" width="16%">
                                    <div class="btn-group btn-group-sm btn-block p-0 m-0">
                                        <a href="usuario?action=editar&id=<%= usuario.getId() %>" class="btn btn-info">Editar</a>
                                        <a href="usuario?action=delete&id=<%= usuario.getId() %>" class="btn btn-danger">Excluir</a>
                                    </div>
                                </td>
                            </tr>
                            <% } %>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>
