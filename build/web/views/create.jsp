<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sistema de Cadastro de Pessoas</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="./css/bootstrap.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="container">
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                      <li class="nav-item active">
                        <a class="nav-link" href="./">Início <span class="sr-only">(current)</span></a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="pessoa?action=listar">Listar Pessoas</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="pessoa?action=create">Adicionar Pessoa</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="usuario?action=listar">Listar Usuários</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="usuario?action=create">Adicionar Usuário</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="login?action=logout">Sair</a>
                      </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="pt-4  pb-3">Adicionar de Pessoas</h1>
                </div>
            </div>
            <form action="pessoa">
                <div class="form-group">
                  <label for="nome">Nome</label>
                  <input type="text" class="form-control" id="nome" name="nome">
                </div>
                <div class="form-group">
                  <label for="telefone">Telefone</label>
                  <input type="text" class="form-control" id="telefone" name="telefone">
                </div>
                <div class="form-group">
                  <label for="email">E-mail</label>
                  <input type="email" class="form-control" id="email" name="email">
                </div>
                
                <input type="submit" name="action" class="btn btn-primary mb-2" value="Salvar">
            </form>
        </div>
    </body>
</html>
