<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% 
HttpSession sessao = request.getSession();
Object val = sessao.getAttribute("usucodigo");
if (val != null) { 
    response.sendRedirect("pessoa?action=listar");
} else {
    
}
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sistema de Cadastro de Pessoas</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-4 offset-4 mt-5">
                    <div class="jumbotron p-5">
                        <h4 class="text-center" style="padding-bottom: 30px;">Cadastro de Pessoas</h4>
                        <form action="login" method="POST">
                            <div class="form-group">
                                <label>E-mail</label>
                                <input type="email" name="email" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label>Senha</label>
                                <input type="password" name="senha" class="form-control" />
                            </div>
                            
                            <input type="submit" value="Logar" class="btn btn-primary btn-block">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
